# Understanding the impact of the addition of opportunist data in population models for marine megafauna

The objective of this project is to create a tool for quantifying the impact on inferences on species distribution by the addition of opportunist data in models currently used to estimate the abundance and distribution of marine megafauna.

This work-flow in R is designed to:
1. Generate a map of simulated abundance in the studied region;
2. Simulate data that would be obtained by a scientific observation campaign over this region;
3. Add simulated data from so-called platform of opportunity (opportunist observations, including citizen science data);
4. Conduct an analysis by Distance Sampling and Density Surface Models on these data and obtain a prediction of the abundance in the studied region;
5. Compare simulated (truth) and predicted abundances.

The goal is to repeat the analysis for a large number of inputs in order to assess model predictive and inferential abilities when adding opportunist data to the model.

For now, the added opportunist data can only take the form of one or more added transects, simulating observations from a "platform of opportunity".
We can take into account effects of over-sampling and decreased observation quality for these data.


## Usage

To use this work-flow, open file "07_Complete_Simulation.r", change the inputs to match the ones you want and simply run the file.

Example of inputs:
```{r}
# CHANGE INPUTS HERE
#------------------------------------------------------------------------------------------
# Seeds for abundance map
seeds = sample.int(1E9, 5)
# Coordinates for added transects (opportunist data)
# Here coordinates are chosen arbitrarily to cross the whole survey area
transect_coordinates = list(c(3300000,3050000,2320000,3050000),
                            c(3400000,2950000,2800000,2700000)
                            )
# Number of times the added transects are sampled
resampling_range = c(1,10)      # no sampling will always be included (i.e. no addition of opportunist data)
# ESW for the added transects
esw_range = c(100,200)
#------------------------------------------------------------------------------------------

```
Here we are randomly sampling 5 map seeds (i.e generating 5 different, random abundance maps), on each map two transects representing opportunist data are added. These transects sampled 0 (by default), 1 and 10 times with an Effective Strip half-Width (esw) of 100m and 200m.

Transects coordinates take the form of a list of vectors where each vector represents the coordinates of the two endpoints of a transect (x1,x2,y1,y2).

Example of outputs:

![Output example](Example_output.PNG)

The output takes the form of a dataframe containing the inputs and the metrics of interest for every different input combination.
Metrics of interest include:
- Frequency of selection of each different covariates in "good" models (here 6 different covariates)
- Best model, the one selected and used for the prediction
- Error % for the total abundance in the studied region
- Mean and Median of the absolute error % for each cell of the studied region
- Earth Mover's Distance (EMD)

Files 00 to 07, as well as the files in "data" (environmental data in the studied region) are required for the work-flow to run.  
File "08_Simulation_Results" is used to analyze the results from a large number of simulations (evolution and tendencies of the metrics of interest).

## Alternative Usage

The simulation and modelisation processes have also been split, in file 09 and 10 respectively.  
It is thus possible to use the work flow by running file "09_Simulation_Process", with the wanted inputs, to generate the distance data.  
We can then run the modelisation process and results comparison by having the "dist_data" dataframe outputted by file 09 in the current working environment, and running file "10_Modelisation_Process".  
We then obtain similar outputs as previously shown.

Splitting the distance data simulation and the modelisation on said data can be useful in some cases, but it is important to note that it requires much more storage capacity. Indeed, the distance data needed to conduct the modelisation process is much large then the actual metrics outputted by the modelisation.  
When using the work-flow normally, the distance data is cleared after the modelisation process is done for a given seed, but here we must save the entire grid of abundance, covariates, as well as every transect info for every different input combinations, yielding very big dataframes very quickly.

We thus recommend using the work-flow as shown in the previous section, unless it is really necessary to split data acquisition and analysis.

## Packages used
List of the R packages needed for this code to run:
- tidyverse
- move
- sf
- sfheaders
- units
- purrr
- mgcv
- rnaturalearth
- rnaturalearthdata
- rlist
- gridExtra
- Distance
- dsm
- dssd (https://github.com/DistanceDevelopment/dssd)
- dsims (https://github.com/DistanceDevelopment/dsims)
- sismow (https://github.com/maudqueroue/sismow)
- pelaStan (https://gitlab.univ-lr.fr/pelaverse/pelastan)

## More info
For more detailed informations on the analysis process used in the work flow, as well as the background and motivations behind this study, please refer to the Master of Science Thesis in folder "MSC" (in French).

## Improvements
Possible improvements include:
- Adding verifications and/or error messages to check input formats and make usage easier
- Taking into account temporal and seasonal effects and not only environmental (spatial) covariates;
- Adding a metric (Gower's distance ?) to quantify the representativity of the opportunist transects compared to the rest of the studied region;
- Allowing the inclusion of other types of opportunist observations (where effort isn't controlled for example).

## Authors and acknowledgment
Work-flow constructed by Vincent BENOIT, with the much-appreciated help of Matthieu AUTHIER.

## Project status
On pause (internship ended).
