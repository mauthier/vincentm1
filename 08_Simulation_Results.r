################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-04
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### Analyzing results from multiple simulations to look at tendencies
# Getting plots and preliminary results

library(tidyverse)
library(gridExtra)


## PREPARING DATA

# Load sim results (dataframe name: 'results')
load(file="output/sim_results.Rda")

# removing rows with values for the different metrics (top 5% for each)
threshold_q <- 0.95
r <- results %>% 
  mutate(EMD = ifelse(EMD > quantile(EMD, probs = threshold_q),quantile(EMD, probs = threshold_q), EMD),
         abundance_error = ifelse(abundance_error > quantile(abundance_error, probs = threshold_q),
                                  quantile(abundance_error, probs = threshold_q), abundance_error),
         mean_error = ifelse(mean_error > quantile(mean_error, probs = threshold_q),
                             quantile(mean_error, probs = threshold_q), mean_error),
         median_error = ifelse(median_error > quantile(median_error, probs = threshold_q),
                               quantile(median_error, probs = threshold_q), median_error)
         )


## PLOTS

# Plotting bivariate kernel density estimation of abundance error vs EMD
r %>% ggplot(aes(abundance_error,EMD)) + 
  geom_density_2d_filled(contour_var="ndensity") + 
  facet_wrap(vars(n_sampling)) + 
  xlab("Abundance error (%)")

# Plotting bivariate kernel density estimation of mean cell error vs EMD
r %>% ggplot(aes(mean_error,EMD)) + 
  geom_density_2d_filled(contour_var="ndensity") + 
  facet_wrap(vars(n_sampling)) +
  xlab("Mean cell error (%)")

# Plotting bivariate kernel density estimation of mean cell error vs median cell error
r %>% ggplot(aes(median_error,mean_error)) + 
  geom_density_2d_filled(contour_var="ndensity") + 
  facet_wrap(vars(n_sampling)) +
  xlab("Median cell error (%)") +
  ylab("Mean cell error (%)")


## Box plots
# Box plots of abundance error for different resampling values
b1 <- boxplot(abundance_error~n_sampling,
        data = r,
        main="Boxplots for abundance error %",
        xlab="Number of Sampling",
        ylab="Abundance Error"
        )

# Box plots of mean cell error for different resampling values
b2 <- boxplot(mean_error~n_sampling,
        data = r,
        main="Boxplots for mean error %",
        xlab="Number of Sampling",
        ylab="Mean error"
)

# Box plots of median cell error for different resampling values
b3 <- boxplot(median_error~n_sampling,
        data = r,
        main="Boxplots for median error %",
        xlab="Number of Sampling",
        ylab="Median Error"
)

# Box plots of EMD for different resampling values
b4 <- boxplot(EMD~n_sampling,
        data = r,
        main="Boxplots for EMD",
        xlab="Number of Sampling",
        ylab="EMD"
)

# Plotting mean and sd of covariable frequency of selection for different resampling values
results %>%
  select(n_sampling, starts_with("freq_")) %>%
  pivot_longer(col = -n_sampling,
               values_to = "freq",
               names_to = "covariable"
               ) %>%
  mutate(covariable = sapply(covariable, function(x) { str_split(x, pattern = "freq_")[[1]][2] }),
         covariable = factor(covariable, levels = c("bathy", "slope", "mSST", "gradSST", "EKE", "NPPV")),
         n_sampling = factor(n_sampling, levels = as.character(c(0, 1, 10)))
         ) %>%
  group_by(n_sampling, covariable) %>%
  summarize(moyenne = mean(freq),
            sd = sd(freq),
            etendue = diff(range(freq))
            ) %>%
  ungroup() %>%
  ggplot(aes(x = covariable, y = sd, group = n_sampling, color = n_sampling, size = moyenne)) +
  geom_point() +
  scale_color_viridis_d() +
  theme_bw() +
  xlab("Covariables") +
  ylab("Standard Deviation")
  

# Plotting mean and sd of covariable frequency of selection for different resampling values
results %>%
  select(n_sampling, starts_with("freq_")) %>%
  pivot_longer(col = -n_sampling,
               values_to = "freq",
               names_to = "covariable"
  ) %>%
  mutate(covariable = sapply(covariable, function(x) { str_split(x, pattern = "freq_")[[1]][2] }),
         covariable = factor(covariable, levels = c("bathy", "slope", "mSST", "gradSST", "EKE", "NPPV")),
         n_sampling = factor(n_sampling, levels = as.character(c(0, 1, 10)))
  ) %>%
  group_by(n_sampling, covariable) %>%
  summarize(moyenne = mean(freq),
            sd = sd(freq),
            etendue = diff(range(freq))
  ) %>%
  ungroup() %>%
  ggplot(aes(x = covariable, y = moyenne, group = n_sampling, color = n_sampling, size = sd)) +
  geom_point() +
  scale_color_viridis_d() +
  theme_bw() +
  xlab("Covariables") +
  ylab("Mean")

