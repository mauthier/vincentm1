################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-04
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### Comparing models with different covariates taken into account (segment level covariates)
# Following the method presented here: http://distancesampling.org/R/vignettes/mexico-analysis.html

library(move)
library(tidyverse)
library(sf)
library(units)
library(dsm)
library(Distance)
library(rlist)
library(gridExtra)


predictors <- c("mSST", "gradSST", "EKE", "NPPV", "Bathy", "Slope")        # all different covariates
nb_max_pred <- 4         # maximum number of covariates considered in the model
intercept <- paste0("~ te(x, y, bs = c('cs','cs'))")
smoothers <- paste("s(", predictors, ", k = 4, bs = 'cs')",
                   sep = ""
                   )
max_cor = 0.5         # maximum correlation allowed
X = segdata %>% 
  st_as_sf() %>%
  st_drop_geometry() %>%
  drop_units() %>%
  drop_na()

# Generating all different covariate combinations
all_x <- lapply(1:nb_max_pred, combn, x = length(predictors))

# Computing cross correlation for each combination
if (nb_max_pred == 1) {
  rm_combn <- c(rep(0, length(predictors) + 1))
} else {
  rm_combn <- lapply(all_x[-1], function(mat) {
    sapply(1:ncol(mat), function(i) {
      rho <- cor(X[, predictors[mat[, i]]])
      diag(rho) <- 0
      return(max(abs(as.numeric(rho))))
    })
  })
  rm_combn <- c(c(rep(0, length(predictors) + 1)), unlist(rm_combn))
}

# Generating formulas
mlist <- function(n, y, predictors) {
  paste(y, apply(
    X = combn(predictors, n),
    MARGIN = 2,
    paste, collapse = " + ")
    , sep = paste(intercept,"+ ", sep = " ")
  )
}

all_mods <- c(
  paste("count", intercept, sep = " "),
  unlist(lapply(1:nb_max_pred, mlist, y = "count", predictors = smoothers))
)

# Removing models with cross correlation higher than the threshold
all_mods <- all_mods[which(rm_combn < max_cor)]


# Computing AIC score for each model
all_AIC <- rep(NA,length(all_mods))

for (i in 1:length(all_mods)){
  model <- dsm(as.formula(all_mods[i]),
               detfc.hn.null,
               segdata,
               obsdata,
               method="REML",
               family = nb()
               )
  
  all_AIC[i] <- AIC(model)
}


# Selecting the best model (minimum AIC) and the "good" models (within 5 points of the minimum AIC)
best <- all_mods[which.min(all_AIC)]
good <- all_mods[which(all_AIC < min(all_AIC) + 5)]


# Calculating the frequency for each covariate to appear in "good" models
# (will be used as a metric of interest to evaluate the quality of the prediction)
cov_freq <- data.frame(
  mSST = sum(str_detect(good,"mSST"))/length(good),
  gradSST = sum(str_detect(good,"gradSST"))/length(good),
  EKE = sum(str_detect(good,"EKE"))/length(good),
  NPPV = sum(str_detect(good,"NPPV"))/length(good),
  Bathy = sum(str_detect(good,"Bathy"))/length(good),
  Slope = sum(str_detect(good,"Slope"))/length(good)
)

print(cov_freq)
print(best)

# Cleaning environment
rm('all_x','model','X','all_AIC','all_mods','i','intercept','max_cor',
   'nb_max_pred','predictors','rm_combn','smoothers','mlist','good')
