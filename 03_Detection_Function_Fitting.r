################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-04
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### Fitting the Detection Function
# Following the method presented here: http://distancesampling.org/R/vignettes/mexico-analysis.html

library(move)
library(tidyverse)
library(sf)
library(units)
library(dsm)
library(Distance)
library(rlist)
library(gridExtra)


# Making results reproducible
set.seed(123)

### Preparing data
# distdata holds the distance sampling data that will be used to fit the detection function
distdata = new_obs

# Find transects' center positions
tr_center <- new_trans$geometry %>% 
  st_centroid() %>% 
  st_coordinates

# segdata holds the segment data: the transects have already been “chopped” into segments
segdata = data.frame(
  Effort = new_trans$effort,
  Transect.Label = new_trans$transect,
  Sample.Label = new_trans$seg_ID,
  sim = new_trans$sim,
  x = tr_center[,1],
  y = tr_center[,2],
  geom = new_trans$geometry
  )

# obsdata links the distance data to the segments
obsdata = data.frame(
  object = new_obs$object_ID,
  Sample.Label = new_obs$seg_ID,
  size = new_obs$size,
  distance = new_obs$distance
  )

# preddata holds the prediction grid (which includes all the necessary covariates)
preddata = gen.abundance

# Find cells' center positions
cell_center <- preddata$geom %>% 
  st_centroid() %>% 
  st_coordinates()

preddata$x = cell_center[,1]
preddata$y = cell_center[,2]
preddata$area = drop_units(st_area(preddata))


## We need to change all covariates from grid-level covariates (in preddata) to segment-level covariates (in segdata)

# For each segment's centroid, find the cell it's in and assign its ID to match the cell's
foo <- function(x) { ifelse(length(x) == 0, NA, x) }
segdata$ID <- segdata %>% 
  st_as_sf() %>% 
  st_centroid() %>% 
  st_within(preddata) %>% 
  lapply(., foo) %>% 
  unlist()

# If its centroid is outside the survey area, delete the segment
segdata <- segdata[complete.cases(segdata[ ,'ID']),]

# Assigning the appropriate covariates to each segment using cell ID
segdata <-left_join(segdata, st_drop_geometry(covariates), by='ID')


### Fitting the detection function

detfc.hn.null<-ds(distdata, max(distdata$distance), key="hn", adjustment=NULL)
summary(detfc.hn.null)

# Plot of the fitted detection function and goodness of fit for the half-normal model.
par(mfrow=c(1,2))
plot(detfc.hn.null, showpoints=FALSE, pl.den=0, lwd=2)
ddf.gof(detfc.hn.null$ddf)

# Cleaning environment
rm('cell_center','distdata','tr_center','foo')
