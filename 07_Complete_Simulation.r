################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-10
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### RUN THIS CODE TO RUN A COMPLETE SIMULATION LOOP

rm(list = ls())

start_time <- Sys.time()

## Every different input
# CHANGE INPUTS HERE
#------------------------------------------------------------------------------------------
# Seeds for abundance map
seeds = sample.int(1E9, 2)
# Coordinates for added transects (opportunist data)
# Here coordinates are chosen arbitrarily to cross the whole survey area
transect_coordinates = list(c(3300000,3050000,2320000,3050000),
                            c(3400000,2950000,2800000,2700000)
                            )
# Number of times the added transects are sampled
resampling_range = c(1,10)      # no sampling will always be included (i.e. no addition of opportunist data)
# ESW for the added transects
esw_range = c(100,200)

# Covariable names and types of impact on abundance ("0" = random, "+" = always increasing, "-" = always decreasing)
covname = c("Bathy", "Slope", "mSST")
monotone = c("0", "0", "0")
#------------------------------------------------------------------------------------------

# Generating every combinations of inputs
cases = expand.grid(seed=seeds, n_sampling=resampling_range, esw=esw_range)


# Preparing result dataframe
columns = c("seed","n_sampling","esw","freq_mSST","freq_gradSST","freq_EKE","freq_NPPV","freq_bathy","freq_slope",
            "best","abundance_error","mean_error","median_error","EMD")
result = data.frame(matrix(nrow = nrow(cases)+length(seeds), ncol = length(columns)))
colnames(result) = columns

n_sim = 0

# Loading useful functions
source("00_Useful_Functions.r")

## Running the simulation for every different case

resampling_range <- resampling_range[order(resampling_range)]   # making sure resampling values are in increasing order

for (seed in seeds){      # looping over seeds and generating abundance map
  map_seed = seed
  
  # Generating abundance map
  source("01_Simulate_Abundance.r")
  
  # Simulating observation data from a dedicated scientific campaign
  source("02_Simulate_Observations.r")
  
  ## Observations with no addition of opportunist data
  
  # Plot for observations
  observation_map <- ggplot() +
    geom_sf(data = map_obj, fill = "#CDDAFD", color = "#CDDAFD") +
    geom_sf(data = transects, color = "black") +
    geom_point(data=obs[obs$detected==0,], aes(x=x, y=y), shape=20, color="#051923", alpha=0.15) +
    geom_point(data=detected, aes(x=x, y=y), shape=21, fill="red", size=2) +
    geom_sf(data = france, fill = "grey", alpha = 0.3, linewidth = 0.25) +
    coord_sf(xlim = c(2900000, 3500000), ylim = c(2300000, 3150000), expand = FALSE) +
    labs(caption = paste(nrow(detected),"total sightings",sep = " ")) +
    theme(legend.position = "none",
          panel.background = element_rect(fill = "white"),
          panel.grid.major = element_line(colour = "#EDEDE9"))
  
  print(observation_map)
  
  # adding results to the dist_data list
  v <- rep(1,nrow(transects))
  transects$sim <- v
  new_trans <- transects
  new_obs <- detected
  
  # Modelisation process
  
  source("03_Detection_Function_Fitting.r") # taking into account imperfect detection
  source("04_Model_Selection.r")            # selecting best model (which covariates to include)
  source("05_Spatial_Model_Fitting.r")      # using best model to make an abundance prediction
  source("06_Result_Comparison.r")          # comparing simulated and predicted abundance, calculating metrics to evaluate quality of prediction
  
  ## Outputs
  n_sim <- n_sim + 1           # keeping track of the sim process
  
  # adding results to the result dataframe
  result$seed[n_sim] = map_seed
  result$n_sampling[n_sim] = 0
  result$esw[n_sim] = 0
  result$freq_mSST[n_sim] = cov_freq[[1]]
  result$freq_gradSST[n_sim] = cov_freq[[2]]
  result$freq_EKE[n_sim] = cov_freq[[3]]
  result$freq_NPPV[n_sim] = cov_freq[[4]]
  result$freq_bathy[n_sim] = cov_freq[[5]]
  result$freq_slope[n_sim] = cov_freq[[6]]
  result$best[n_sim] = best
  result$abundance_error[n_sim] = abundance_error
  result$mean_error[n_sim] = mean_cell_error
  result$median_error[n_sim] = median_cell_error
  result$EMD[n_sim] = EMD
  
  # Show where we are in the simulation process
  print(paste("Simulation number:",n_sim,"/",nrow(cases)+length(seeds)))
  end_time <- Sys.time()
  print(paste("Time since start of sim:",end_time-start_time))
  
  ## Observations with added opportunist data
  
  for(esw in esw_range){       # looping over esw
    tr_esw = esw
    
    for (n_resamp in 1:length(resampling_range)){     # looping over resampling values and adding transects and observations
      resampling = resampling_range[n_resamp]
      
      # Adding transects and observations from citizen science using the add_detections function
      if (n_resamp == 1){
        new_trans <- transects
        new_obs <- detected
        for(coordinates in transect_coordinates){
          res = add_detections(new_trans,coordinates,resampling,new_obs,map_obj,group_size,N_ind,truncation,tr_esw)
          new_trans <- res$new_trans
          new_obs <- res$new_detections
        }
      } else {
        for(coordinates in transect_coordinates){
          res = add_detections(new_trans,coordinates,resampling_range[n_resamp]-resampling_range[n_resamp-1],
                               new_obs,map_obj,group_size,N_ind,truncation,tr_esw)
          new_trans <- res$new_trans
          new_obs <- res$new_detections 
        }
      }
      
      # Plot for observations
      observation_map <- ggplot() +
        geom_sf(data = map_obj, fill = "#CDDAFD", color = "#CDDAFD") +
        geom_sf(data = new_trans[new_trans$sim==1,], color = "black") +
        geom_sf(data = new_trans[new_trans$sim==0,], color = "blue", linewidth=1) +
        geom_point(data=obs[obs$detected==0,], aes(x=x, y=y), shape=20, color="#051923", alpha=0.15) +
        geom_point(data=new_obs, aes(x=x, y=y), shape=21, fill="red", size=2) +
        geom_sf(data = france, fill = "grey", alpha = 0.3, linewidth = 0.25) +
        coord_sf(xlim = c(2900000, 3500000), ylim = c(2300000, 3150000), expand = FALSE) +
        labs(caption = paste(nrow(new_obs),"total sightings, including",nrow(new_obs)-nrow(detected),"from opportunist data",sep = " ")) +
        theme(legend.position = "none",
              panel.background = element_rect(fill = "white"),
              panel.grid.major = element_line(colour = "#EDEDE9"))
      
      print(observation_map)
      
      # Modelisation process
    
      source("03_Detection_Function_Fitting.r") # taking into account imperfect detection
      source("04_Model_Selection.r")            # selecting best model (which covariates to include)
      source("05_Spatial_Model_Fitting.r")      # using best model to make an abundance prediction
      source("06_Result_Comparison.r")          # comparing simulated and predicted abundance, calculating metrics to evaluate quality of prediction
      
      ## Outputs
      n_sim <- n_sim + 1           # keeping track of the sim process
      
      # adding results to the result dataframe
      result$seed[n_sim] = map_seed
      result$n_sampling[n_sim] = resampling
      result$esw[n_sim] = tr_esw
      result$freq_mSST[n_sim] = cov_freq[[1]]
      result$freq_gradSST[n_sim] = cov_freq[[2]]
      result$freq_EKE[n_sim] = cov_freq[[3]]
      result$freq_NPPV[n_sim] = cov_freq[[4]]
      result$freq_bathy[n_sim] = cov_freq[[5]]
      result$freq_slope[n_sim] = cov_freq[[6]]
      result$best[n_sim] = best
      result$abundance_error[n_sim] = abundance_error
      result$mean_error[n_sim] = mean_cell_error
      result$median_error[n_sim] = median_cell_error
      result$EMD[n_sim] = EMD
      
      # Show where we are in the simulation process
      print(paste("Simulation number:",n_sim,"/",nrow(cases)+length(seeds)))
      end_time <- Sys.time()
      print(paste("Time since start of sim:",end_time-start_time))
      
      # Cleaning memory
      gc()
    }
  }
  
  # Cleaning environment
  rm('bob','N_ind','group_size','gen.abundance','new_obs','new_trans', 'covariates','gen.density','obs',
     'gen.abundance.final', 'cov_freq','observation_map','p1','p2','p3','prediction.final','abundance_error','detected','best'
     ,'EMD','gen.ind','mean_cell_error','median_cell_error','pred.abundance','map_obj','france','crs','truncation','end_time',
     'ind','res','shape_obj','transects')
  
  # Saving Sim Results
  # save(result,file="output/readme_example.Rda")
}

view(result)
