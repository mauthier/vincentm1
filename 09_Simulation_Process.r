################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-10
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### Loop to generate all distance data
# Distance data can then be used for modelisation

rm(list = ls())

start_time <- Sys.time()

## Every different input
# CHANGE INPUTS HERE
#------------------------------------------------------------------------------------------
# Seeds for abundance map
seeds = sample.int(1E9, 2)
# Coordinates for added transects (opportunist data)
# Here coordinates are chosen arbitrarily to cross the whole survey area
transect_coordinates = list(c(3300000,3050000,2320000,3050000),
                            c(3400000,2950000,2800000,2700000)
                            )
# Number of times the added transects are sampled
resampling_range = c(1,10)      # no sampling will always be included (i.e. no addition of opportunist data)
# ESW for the added transects
esw_range = c(100,200)
#------------------------------------------------------------------------------------------

# Generating every combinations of inputs
cases = expand.grid(seed=seeds, n_sampling=resampling_range, esw=esw_range)


# Preparing dist_data results
dist_data = vector("list",nrow(cases)+length(seeds))

n_sim = 0

# Loading useful functions
source("00_Useful_Functions.r")

## Running the simulation for every different case

resampling_range <- resampling_range[order(resampling_range)]   # making sure resampling values are in increasing order

for (seed in seeds){      # looping over seeds and generating abundance map
  map_seed = seed
  
  # Generating abundance map
  source("01_Simulate_Abundance.r")
  
  # Simulating observation data from a dedicated scientific campaign
  source("02_Simulate_Observations.r")
  
  ## Observations with no addition of opportunist data
  
  # Plot for observations
  observation_map <- ggplot() +
    geom_sf(data = map_obj, fill = "#CDDAFD", color = "#CDDAFD") +
    geom_sf(data = transects, color = "black") +
    geom_point(data=obs[obs$detected==0,], aes(x=x, y=y), shape=20, color="#051923", alpha=0.15) +
    geom_point(data=detected, aes(x=x, y=y), shape=21, fill="red", size=2) +
    geom_sf(data = france, fill = "grey", alpha = 0.3, linewidth = 0.25) +
    coord_sf(xlim = c(2900000, 3500000), ylim = c(2300000, 3150000), expand = FALSE) +
    labs(caption = paste(nrow(detected),"total sightings",sep = " ")) +
    theme(legend.position = "none",
          panel.background = element_rect(fill = "white"),
          panel.grid.major = element_line(colour = "#EDEDE9"))
  
  print(observation_map)
  
  ## Outputs
  n_sim <- n_sim + 1           # keeping track of the sim process
  
  # adding results to the dist_data list
  v <- rep(1,nrow(transects))
  transects$sim <- v
  
  sim_result = list(
    "seed" = seed,
    "esw" = 0,
    "n_sampling" = 0,
    "gen_ind" = gen.ind,
    "abundance" = gen.abundance,
    "obs" = detected,
    "trans" = transects
  )
  
  dist_data[[n_sim]] <- sim_result
  
  # Show where we are in the simulation process
  print(paste("Simulation number:",n_sim,"/",nrow(cases)+length(seeds)))
  end_time <- Sys.time()
  print(paste("Time since start of sim:",end_time-start_time))
  
  ## Observations with added opportunist data
  
  for(esw in esw_range){       # looping over esw
    tr_esw = esw
    
    for (n_resamp in 1:length(resampling_range)){     # looping over resampling values and adding transects and observations
      resampling = resampling_range[n_resamp]
      
      # Adding transects and observations from citizen science using the add_detections function
      if (n_resamp == 1){
        new_trans <- transects
        new_obs <- detected
        for(coordinates in transect_coordinates){
          res = add_detections(new_trans,coordinates,resampling,new_obs,map_obj,group_size,N_ind,truncation,tr_esw)
          new_trans <- res$new_trans
          new_obs <- res$new_detections
        }
      } else {
        for(coordinates in transect_coordinates){
          res = add_detections(new_trans,coordinates,resampling_range[n_resamp]-resampling_range[n_resamp-1],
                               new_obs,map_obj,group_size,N_ind,truncation,tr_esw)
          new_trans <- res$new_trans
          new_obs <- res$new_detections 
        }
      }
      
      # Plot for observations
      observation_map <- ggplot() +
        geom_sf(data = map_obj, fill = "#CDDAFD", color = "#CDDAFD") +
        geom_sf(data = new_trans[new_trans$sim==1,], color = "black") +
        geom_sf(data = new_trans[new_trans$sim==0,], color = "blue", linewidth=1) +
        geom_point(data=obs[obs$detected==0,], aes(x=x, y=y), shape=20, color="#051923", alpha=0.15) +
        geom_point(data=new_obs, aes(x=x, y=y), shape=21, fill="red", size=2) +
        geom_sf(data = france, fill = "grey", alpha = 0.3, linewidth = 0.25) +
        coord_sf(xlim = c(2900000, 3500000), ylim = c(2300000, 3150000), expand = FALSE) +
        labs(caption = paste(nrow(new_obs),"total sightings, including",nrow(new_obs)-nrow(detected),"from opportunist data",sep = " ")) +
        theme(legend.position = "none",
              panel.background = element_rect(fill = "white"),
              panel.grid.major = element_line(colour = "#EDEDE9"))
      
      print(observation_map)
      
      ## Outputs
      n_sim <- n_sim + 1           # keeping track of the sim process
      
      # adding results to the dist_data list
      
      sim_result = list(
        "seed" = seed,
        "esw" = tr_esw,
        "n_sampling" = resampling,
        "gen_ind" = gen.ind,
        "abundance" = gen.abundance,
        "obs" = new_obs,
        "trans" = new_trans
      )
      
      dist_data[[n_sim]] <- sim_result
      
      # Show where we are in the simulation process
      print(paste("Simulation number:",n_sim,"/",nrow(cases)+length(seeds)))
      end_time <- Sys.time()
      print(paste("Time since start of sim:",end_time-start_time))
      
      # Cleaning memory
      gc()
    }
  }
  
  # Cleaning environment
  rm('bob','N_ind','group_size','gen.abundance','new_obs','new_trans','gen.density','detected',
     'ind','map_obj','obs','observation_map','res','shape_obj','sim_result','transects','crs','end_time','esw',
     'gend.ind','map_seed','resampling','tr_esw','truncation','v')
  
  # Saving Dist Data Results
  #save(dist_data,file="output/dist_data_01.Rda")
}

# Cleaning environment
rm('cases','esw_range','gen.ind','n_resamp','n_sim','resampling_range','seed','seeds','start_time','add_detections', 'make_distri')

