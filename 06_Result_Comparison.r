################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-04
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
#
################################################################################

### Result Analysis, plots, etc...
# Computing metrics of interest for a given simulation to evaluate the quality of the prediction

# Plotting generated abundance map, predicted abundance map, and error map
max = max(prediction.final$abundance,gen.abundance.final$abundance)

p1 <- gen.abundance.final %>%      # simulated abundance
  ggplot() +
  geom_sf(aes(geometry=geom, fill = abundance), color = NA) +
  geom_sf(data = france, fill = "grey", alpha = 0.3, linewidth = 0.25) +
  coord_sf(xlim = c(2900000, 3500000), ylim = c(2300000, 3150000), expand = FALSE) +
  scale_fill_viridis_c(trans = "sqrt", breaks=c(0, round(0.05*max), round(0.2*max), round(0.5*max) , ceiling(max)),
                       limits = c(0, ceiling(max))) +
  theme_bw()

p2 <- ggplot() +          # predicted abundance
  geom_sf(data=prediction.final, aes(geometry = geometry, fill = abundance), color= NA) +
  geom_sf(data = france, fill = "grey", alpha = 0.3, linewidth = 0.25) +
  coord_sf(xlim = c(2900000, 3500000), ylim = c(2300000, 3150000), expand = FALSE) +
  scale_fill_viridis_c(trans = "sqrt", breaks=c(0, round(0.05*max), round(0.2*max), round(0.5*max) , ceiling(max)),
                       limits = c(0, ceiling(max))) +
  theme_bw()

max_error = max(abs(prediction.final$difference))

p3 <- ggplot() +          # error map
  geom_sf(data=prediction.final, aes(geometry = geometry, fill = difference), color= NA) +
  geom_sf(data = france, fill = "grey", alpha = 0.3, linewidth = 0.25) +
  coord_sf(xlim = c(2900000, 3500000), ylim = c(2300000, 3150000), expand = FALSE) +
  scale_fill_gradient2(low = "blue",high = "red",limits = c(-ceiling(max_error), ceiling(max_error))) +
  theme_bw()

grid.arrange(p1, p2, p3, nrow = 2)

## Error calculations
# Generated total abundance
print(paste("Total generated abundance:", gen.ind))

# Predicted total abundance
pred.abundance <- round(sum(prediction.final$abundance))
print(paste("Total predicted abundance:", pred.abundance))

# Compute error percentage for total abundance
abundance_error <- round((sum(prediction.final$abundance)-gen.ind)*100/(gen.ind),2)
print(paste("Error for total abundance:", abundance_error, "%"))

# Compute mean and median absolute error percentage for each cell
mean_cell_error <- round(mean(prediction.final$error.pct),2)
print(paste("Mean cell error:", mean_cell_error, "%"))

median_cell_error <- round(median(prediction.final$error.pct),2)
print(paste("Median cell error:", median_cell_error, "%"))


### Earth Mover's Distance

# Preparing data
pred.prob <- data.frame(
  proba = prediction.final$abundance/sum(prediction.final$abundance)
)

pred.prob <- SpatialPointsDataFrame(st_coordinates(st_centroid(prediction.final$geometry)),pred.prob)

gen.prob <- data.frame(
  proba = gen.abundance.final$abundance/sum(gen.abundance.final$abundance)
)

gen.prob <- SpatialPointsDataFrame(st_coordinates(st_centroid(gen.abundance.final$geom)),gen.prob)

# Calculating EMD
EMD <- round(emd(gen.prob, pred.prob, gc = TRUE, threshold = 10000)[1],2)
print(paste("Earth Mover's Distance:", EMD))


# Cleaning environment
rm('gen.prob','pred.prob','max','max_error')
