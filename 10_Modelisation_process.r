################################################################################
### Description
# Auteurs: Matthieu AUTHIER, Vincent BENOIT
# Dernière modif: 2023-07-10
# R version 4.2.3 (2023-03-15 ucrt) -- "Shortstop Beagle"
# Copyright (C) 2023 The R Foundation for Statistical Computing
# Platform: x86_64-w64-mingw32/x64 (64-bit)
#
################################################################################

### Run modelisation process from already existing distance data

start_time <- Sys.time()

# Load dist data (dataframe name: 'dist_data')
load(file="output/dist_data_test.Rda")


# Preparing result dataframe
columns = c("seed","n_sampling","esw","freq_mSST","freq_gradSST","freq_EKE","freq_NPPV","freq_bathy","freq_slope",
            "best","abundance_error","mean_error","median_error","EMD")
result = data.frame(matrix(nrow = length(dist_data), ncol = length(columns)))
colnames(result) = columns

# Load France shape
france <- ne_countries(scale = 'medium', returnclass = "sf",country="france") %>% 
  st_transform(3035)

# Load covariates map (dataframe name: 'covariates')
load(file="data/covariates_bob.Rda")

# Looping over every lign in dist_data and running modelisation process

for (n_sim in 1:length(dist_data)){
  gen.ind = dist_data[[n_sim]]$gen_ind
  gen.abundance = dist_data[[n_sim]]$abundance
  new_obs = dist_data[[n_sim]]$obs
  new_trans = dist_data[[n_sim]]$trans
  
  # Modelisation process
  
  source("03_Detection_Function_Fitting.r") # taking into account imperfect detection
  source("04_Model_Selection.r")            # selecting best model (which covariates to include)
  source("05_Spatial_Model_Fitting.r")      # using best model to make an abundance prediction
  source("06_Result_Comparison.r")          # comparing simulated and predicted abundance, calculating metrics to evaluate quality of prediction
  
  # adding results to the result dataframe
  result$seed[n_sim] = dist_data[[n_sim]]$seed
  result$n_sampling[n_sim] = dist_data[[n_sim]]$n_sampling
  result$esw[n_sim] = dist_data[[n_sim]]$esw
  result$freq_mSST[n_sim] = cov_freq[[1]]
  result$freq_gradSST[n_sim] = cov_freq[[2]]
  result$freq_EKE[n_sim] = cov_freq[[3]]
  result$freq_NPPV[n_sim] = cov_freq[[4]]
  result$freq_bathy[n_sim] = cov_freq[[5]]
  result$freq_slope[n_sim] = cov_freq[[6]]
  result$best[n_sim] = best
  result$abundance_error[n_sim] = abundance_error
  result$mean_error[n_sim] = mean_cell_error
  result$median_error[n_sim] = median_cell_error
  result$EMD[n_sim] = EMD
  
  # Show where we are in the simulation process
  print(paste("Simulation number:",n_sim,"/",length(dist_data)))
  end_time <- Sys.time()
  print(paste("Time since start of sim:",end_time-start_time))
  
  # Cleaning memory
  gc()
  
  # Cleaning environment
  rm('gen.abundance.final', 'cov_freq','p1','p2','p3','prediction.final','abundance_error','best',
     'EMD','gen.ind','mean_cell_error','median_cell_error','pred.abundance','end_time')
  
  # Saving Sim Results
  save(result,file="output/modelisation_test.Rda")
}


view(result)