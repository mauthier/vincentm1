##--------------------------------------------------------------------------------------------------------
## SCRIPT : Simulate species distribution maps in BoB
##
## Authors : Matthieu AUTHIER, Vincent BENOIT
## Last update : 2023-07-04
##
## R version 4.2.2 (2022-10-31 ucrt) -- "Innocent and Trusting"
## Copyright (C) 2022 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

### Generate an abundance map with covariates gradSST, mSST, EKE, NPPV, Bathy and Slope in the Bay of Biscay


lapply(c("tidyverse", "purrr", "sf", "units", "sismow", "mgcv","rnaturalearth"),
       library, character.only = TRUE
       )

# Load grid map and environmental data
grid <- st_read("./data/20230509_envdata2019.gpkg", layer = "grid")
envdata <- st_read("./data/20230509_envdata2019.gpkg", layer = "envdata") %>%
  group_by(Month) %>%
  mutate(ID = 1:n()) %>%
  ungroup()

# Load France shape
france <- ne_countries(scale = 'medium', returnclass = "sf",country="france") %>% 
  st_transform(3035)

# Generate distribution using the make_distri function
gen.density <- make_distri(sfdf = envdata,
                    grid = grid,
                    mean_density = 1,
                    covname = covname,
                    monotone = monotone,
                    seed = map_seed
                    )

# Generating Shape object for the Bay of Biscay
bob <- st_sf(ID = "BoB",
             grid %>%
               st_union()
             )

# Converting density to abundance
gen.abundance <- gen.density$distribution %>%
  mutate(abundance = drop_units(density*st_area(geom)))


### Adding covariates to gen.abundance

# Creating a data frame with only cell IDs and covariates
covariates <- envdata %>% 
  filter(Month==7) %>% 
  select(c('ID','mSST','gradSST','EKE','NPPV','Bathy','Slope'))

# For each cell in gen.abundance, find its centroid, find the cell (in covariates) it is in and assign its ID to match it
foo <- function(x) { ifelse(length(x) == 0, NA, x) }
gen.abundance$ID <- gen.abundance %>% 
  st_as_sf() %>% 
  st_centroid() %>% 
  st_within(covariates) %>% 
  lapply(., foo) %>% 
  unlist()

# If its centroid is outside the survey area, delete the cell
gen.abundance <- gen.abundance[complete.cases(st_drop_geometry(gen.abundance[ ,'ID'])),]


# Assigning the appropriate covariates to each segment using cell ID
gen.abundance <-left_join(gen.abundance, st_drop_geometry(covariates), by='ID')

# Re-assigning cell ID to avoid duplicates
gen.abundance$ID = 1:nrow(gen.abundance)


# Cleaning environment
rm('envdata', 'grid', 'foo','abundance_map')
